package hcom.com.homework.di.components;

import javax.inject.Singleton;

import dagger.Component;
import hcom.com.homework.HWApplication;
import hcom.com.homework.di.modules.ApplicationModule;

/**
 * Created by Jozsef_Szele on 2016.12.16..
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(HWApplication application);

    final class Injector {

        private Injector() {
        }

        public static ApplicationComponent buildComponent(HWApplication application) {
            return DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(application)).build();
        }

    }

}
