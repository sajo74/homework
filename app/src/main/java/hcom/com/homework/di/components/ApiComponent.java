package hcom.com.homework.di.components;

import javax.inject.Singleton;

import dagger.Component;
import hcom.com.homework.di.scopes.PerActivity;

/**
 * Created by Jozsef_Szele on 2016.12.19..
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class)
public interface ApiComponent {
}
