package hcom.com.homework.di.modules;

import javax.inject.Singleton;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import hcom.com.homework.HWApplication;

/**
 * Created by Jozsef_Szele on 2016.12.16..
 */
@Module
public class ApplicationModule {

    private final HWApplication application;

    public ApplicationModule(HWApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application;
    }
}
