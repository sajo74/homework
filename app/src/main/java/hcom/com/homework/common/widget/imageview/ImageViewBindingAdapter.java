package hcom.com.homework.common.widget.imageview;

import com.bumptech.glide.Glide;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

/**
 * Created by Jozsef_Szele on 2017.01.05..
 */

public class ImageViewBindingAdapter {

    public ImageViewBindingAdapter() {
    }

    @BindingAdapter({"imageContent"})
    public static void setImageUri(ImageView view, String imageUri) {
        //        if (imageUri == null) {
        //            view.setImageURI((Uri) null);
        //        } else {
        //            //view.setImageURI(Uri.parse(imageUri));
        //
        //        }

        Glide.with(view.getContext()).load(imageUri).into(view);

    }

}
