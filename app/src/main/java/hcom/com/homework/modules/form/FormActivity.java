package hcom.com.homework.modules.form;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import hcom.com.homework.R;

/**
 * Created by Jozsef_Szele on 2016.12.15..
 */

public class FormActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form);
    }
}
