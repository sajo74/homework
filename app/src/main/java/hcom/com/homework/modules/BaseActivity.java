package hcom.com.homework.modules;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.widget.FrameLayout;

import hcom.com.homework.R;

/**
 * Created by Jozsef_Szele on 2017.01.02..
 */

public abstract class BaseActivity extends Activity {

    private ViewDataBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity_layout);

        FrameLayout baseLayout = (FrameLayout) findViewById(R.id.base_container);

        dataBinding = DataBindingUtil.inflate(getLayoutInflater(), getLayoutId(), baseLayout, true);
        if (dataBinding != null) {
            applyDataBinding(dataBinding);
        }
    }

    protected abstract int getLayoutId();

    protected void applyDataBinding(ViewDataBinding dataBinding) {

    }

}
