package hcom.com.homework.modules.list.router;

/**
 * Created by Jozsef_Szele on 2017.01.06..
 */

public interface ListRouter {

    void openDetails(String imdbId);
}
