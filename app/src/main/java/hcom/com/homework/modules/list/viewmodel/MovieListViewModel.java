package hcom.com.homework.modules.list.viewmodel;

import java.util.List;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import hcom.com.homework.BR;
import hcom.com.homework.common.widget.recyclerview.ListItemViewModel;


/**
 * Created by Jozsef_Szele on 2017.01.03..
 */

public class MovieListViewModel extends BaseObservable {

    //@Bindable
    // private String headerText;

    // public final ObservableField<String> headerText = new ObservableField<>();

    @Bindable
    private List<ListItemViewModel> movieItemViewModels;

    //    public String getHeaderText() {
    //        return headerText;
    //    }
    //
    //    public void setHeaderText(String headerText) {
    //        this.headerText = headerText;
    //        // notifyPropertyChanged(BR.headerText);
    //    }

    public List<ListItemViewModel> getMovieItemViewModels() {
        return movieItemViewModels;
    }

    public void setMovieItemViewModels(List<ListItemViewModel> movieItemViewModels) {
        this.movieItemViewModels = movieItemViewModels;
        notifyPropertyChanged(BR.movieItemViewModels);
    }

    //    public void onClickHeader(View view) {
    //        Toast.makeText(view.getContext(), "asasasas", Toast.LENGTH_SHORT).show();
    //        //setHeaderText("22222");
    //
    //    }

}
