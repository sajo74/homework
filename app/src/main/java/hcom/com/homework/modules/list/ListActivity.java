package hcom.com.homework.modules.list;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.os.Bundle;

import hcom.com.homework.R;
import hcom.com.homework.common.widget.recyclerview.ListItemViewModel;
import hcom.com.homework.databinding.MainBinding;
import hcom.com.homework.modules.BaseActivity;
import hcom.com.homework.modules.details.DetailsActivity;
import hcom.com.homework.modules.list.router.ListRouter;
import hcom.com.homework.modules.list.viewmodel.MovieListItemViewModel;
import hcom.com.homework.modules.list.viewmodel.MovieListViewModel;

// import hcom.com.homework.BR;

public class ListActivity extends BaseActivity implements ListRouter {

    public static final String INTENT_EXTRA_IMDB_ID = "imdb_id";
    private MovieListViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.main);

        //        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.listview);
        //        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        //        recyclerView.setLayoutManager(layoutManager);
        //        recyclerView.setHasFixedSize(true);
        //        RecyclerView.Adapter adapter = new HWListAdapter(getItems());
        //        recyclerView.setAdapter(adapter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.main;
    }

    @Override
    protected void applyDataBinding(ViewDataBinding dataBinding) {
        super.applyDataBinding(dataBinding);
        viewModel = new MovieListViewModel();

        viewModel.setMovieItemViewModels(getItems());

        // viewModel.setHeaderText(getString(R.string.list_header));
        // viewModel.headerText.set(getString(R.string.list_header));
        // dataBinding.setVariable(BR.viewModel, viewModel);

        MainBinding binding = (MainBinding) dataBinding;
        binding.setViewModel(viewModel);

    }

    @Override
    public void openDetails(String imdbId) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(INTENT_EXTRA_IMDB_ID, imdbId);
        startActivity(intent);
    }

    private List<ListItemViewModel> getItems() {
        List<ListItemViewModel> items = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            MovieListItemViewModel model = new MovieListItemViewModel(this);
            model.setTitle("Star wars " + i);
            model.setYear("" + 1977 + i);
            if (i % 2 == 0) {
                model.setImgUrl("https://images-na.ssl-images-amazon.com/images/M/MV5BMjNkMzc2N2QtNjVlNS00ZTk5LTg0MTgtODY2MDAwNTMwZjBjXkEyXkFqcGdeQXVyNDk3NzU2MTQ@._V1_SX300.jpg");
            } else {
                model.setImgUrl("http://ia.media-imdb.com/images/M/MV5BMzM0Nzk2MzkyOF5BMl5BanBnXkFtZTcwMDgxMjM4NA@@._V1_SX300.jpg");
            }
            items.add(model);
        }
        return items;
    }

}
