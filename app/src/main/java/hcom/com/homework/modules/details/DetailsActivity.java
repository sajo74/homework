package hcom.com.homework.modules.details;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import hcom.com.homework.R;
import hcom.com.homework.modules.form.FormActivity;

/**
 * Created by Jozsef_Szele on 2016.12.15..
 */

public class DetailsActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);

        Button button = (Button) findViewById(R.id.details_edit_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsActivity.this, FormActivity.class);
                DetailsActivity.this.startActivity(intent);
            }
        });

    }

}
