package hcom.com.homework.modules.list.viewmodel;

import android.databinding.Bindable;
import android.view.View;

import com.bumptech.glide.Glide;

import hcom.com.homework.common.widget.recyclerview.ListItemViewModel;
import hcom.com.homework.modules.list.router.ListRouter;

/**
 * Created by Jozsef_Szele on 2016.12.16..
 */

public class MovieListItemViewModel extends ListItemViewModel {

    private String title;
    private String year;
    private String imgUrl;
    private ListRouter listRouter;

    public MovieListItemViewModel(ListRouter listRouter) {
        this.listRouter = listRouter;
    }

    public void onClick(View view) {
        listRouter.openDetails(title);
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Bindable
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Bindable
    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @Override
    public int getViewType() {
        return 0;
    }
}
