package hcom.com.homework;

import android.app.Application;

import hcom.com.homework.di.components.ApplicationComponent;

/**
 * Created by Jozsef_Szele on 2016.12.16..
 */

public class HWApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationComponent.Injector.buildComponent(this).inject(this);
    }
}
